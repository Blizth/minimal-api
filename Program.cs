using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<ArticleRepository>();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Minimal APIs Example");
    c.RoutePrefix = "";
});

app.MapGet("/article", (ArticleRepository repository) => repository.GetAll())
    .Produces<IList<Article>>(StatusCodes.Status200OK);

app.MapGet("/article/{id}", (int id, ArticleRepository repository)
    => repository.GetById(id) is Article article ? Results.Ok(article) : Results.NotFound())
    .Produces<Article>(StatusCodes.Status200OK)
    .Produces(StatusCodes.Status404NotFound);

app.MapPost("/article", ([FromServices] ArticleRepository repository, [FromBody] Article article) =>
{
    repository.Add(article);
    return Results.Created($"/article/{article.id}", article);
})
    .Produces(StatusCodes.Status201Created);

app.MapPut("/article", (ArticleRepository repository, Article article) =>
{
    repository.Edit(article);
    return Results.NoContent();
})
    .Produces(StatusCodes.Status204NoContent);

app.MapDelete("/article", (ArticleRepository repository, int id) =>
{
    repository.Remove(id);
    return Results.NoContent();
})
    .Produces(StatusCodes.Status204NoContent);

app.Run();


public record Article(int id, string title, string author);

public class ArticleRepository
{
    private readonly List<Article> _articles = new List<Article>();

    public Article? GetById(int id) => _articles.FirstOrDefault(article => article.id == id);
    public IList<Article> GetAll() => _articles;
    public void Add(Article article) => _articles.Add(article);
    public void Edit(Article article)
    {
        int index = _articles.FindIndex(item => item.id == article.id);
        _articles[index] = article;
    }
    public void Remove(int id)
    {
        int index = _articles.FindIndex(article => article.id == id);
        _articles.RemoveAt(index);
    }
}